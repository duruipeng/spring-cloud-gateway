@echo off
setlocal
set jar="%~dp0%cfa.jar"
set java=java.exe
if defined JDK_HOME (
    set java=%JDK_HOME%\bin\java.exe
)
%java% -jar %jar% %*
endlocal